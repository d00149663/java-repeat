
public class Game {
	private String name;
	private static String genre;
	private int players, age;
	private  boolean network;
	
	public enum genre
	{
			ADVENTURE, ARCADE, SCI_FI, RPG, ACTION, RACING;
			public String toString()
			{
				if (this.ordinal() == 0);
				return genre;
			}			
	}
	
	public Game() {
		this.name = "";
		this.genre = "";
		this.players = 1;
		this.age = 12;
		this.network = false;
	}
	public Game(String name, String genre, int players, int age, boolean network) {
		this.name = name;
		this.genre = genre;
		this.players = players;
		this.age = age;
		this.network = network;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getPlayers() {
		return players;
	}
	public void setPlayers(int players) {
		this.players = players;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public boolean isNetwork() {
		return network;
	}
	public void setNetwork(boolean network) {
		this.network = network;
	}
	@Override
	public String toString() {
		return "Game [name = " + name + ", genre = " + genre + ", players = "
				+ players + ", age = " + age + ", network = " + network + "]";
	}

	

}
