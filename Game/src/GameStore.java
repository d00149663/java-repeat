
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashSet;
import java.util.Set;

/**
 * @author masood
 * AOOD Repeat CA
 *
 */
public class GameStore 
{
	private Set<Game> list;
	
	public GameStore()
	{
		this.list = new HashSet<Game>();
	}
	/**
	 * @param g
	 * add to list
	 */
	public void add(Game g)
	{
		if(!list.contains(g))
		{
			list.add(g);
		}
	}
	/**
	 * @param contents
	 * add game
	 */
	public void add(String contents)
	{
		Game g = search(contents);
		if(g == null)
		{
			list.add(g);
		System.out.println("completed added");
		}
		else
			System.out.println("\nCannot find the game to delete");
	}
	/**
	 * @param contents
	 * delete a game in the list
	 */
	public void delete(String contents)
	{
		Game g = search(contents);
		if(g != null)
		{
			list.remove(g);
		System.out.println("completed deletion");
		}
		else
			System.out.println("\nCannot find the game to delete");
	}
	/**
	 * @param name
	 * @return game name
	 */
	public Game search(String name)
	{
		for(Game g : list)
		{
			if(g.getName().equalsIgnoreCase(name))
			{
				return g;
			}
		}
		return null;
	}
	/**
	 * @param contents
	 * game name change(String) 
	 * string this was first edit
	 */
	public void edit(String contents)
	{		
		Game g = search(contents);
		String name;
		if(g != null)
		{
			name = MyScanner.getString("Enter name to change for the given game: ");
			g.setName(name);
			System.out.println(g);			
		}		
	}	
	/**
	 * @param record
	 * @param stream
	 * @throws IOException
	 * edit game using RAF
	 */
	public void edit(int record,RandomAccessFile stream) throws IOException
	{		
		int num = (record - 1) * 113;
		Game editGame = new Game();
		
		stream.seek(num);
		
		editGame.read(stream);
		editGame.print();
		editGame.setName(MyScanner.getString(("\nEnter name for the game: ")));
		stream.seek(num);
		editGame.write(stream);
		
	}	
	/**
	 * @return size
	 */
	public int size()
	{
		return list.size();
	}
	/**
	 * clear
	 */
	public void clear()
	{
		list.clear();
	}
	
	/**
	 * print items
	 */
	public void print()
	{
		for(Game g : list)
		{
			System.out.println(g);
		}
	}

	/**
	 * @param contents
	 * print ONE item
	 */
	public void printOne(String contents)
	{
		Game g = search(contents);
		if(g != null)
		{			
		System.out.println(g);
		}
		else
			System.out.println("Cannot find the game");
	}
	/**
	 * populate games
	 */
	public void populate()
	{
		Game g1 = new Game("Rayman", "Simulator", 1, 10, false);
		Game g2 = new Game("Mario", "RPG", 2, 12, false);
		Game g3 = new Game("Batman", "Adventure", 1, 18, false);
		Game g4 = new Game("StreetFighter", "Fighting", 2, 12, true);
		Game g5 = new Game("Spiderman", "Adventure", 1, 16, false);
		
		this.add(g1);
		this.add(g2);
		this.add(g3);
		this.add(g4);
		this.add(g5);
		
	}
	/**
	 * @param stream
	 * @throws IOException
	 * save all using RAF
	 */
	public void saveAll(RandomAccessFile stream) throws IOException
	{
		for(Game g : list)
		{
			g.write(stream);
			
		}
	}
	/**
	 * @param stream
	 * @throws IOException
	 * Load all using RAF
	 */
	public void loadAll(RandomAccessFile stream) throws IOException
	{
		long length = stream.length();
		
		long totalgames = length/Game.GAME_LENGTH;
		System.out.println(totalgames);
		for(long i = 0; i < totalgames; i++)
		{
			Game g= new Game();
			g.read(stream);
			System.out.println(g.toString());
			this.add(g);
			
		}
	}
	/**
	 * @param regex
	 * using regex to find game name
	 */
	public void gameSearchRegex(String regex)
	{
		String gameName;
		for(Game g : list)
		{
			
			gameName = g.getName();			
			RegexUtil.print(regex, gameName,0,0);					
		}
		
	}
	/**
	 * @param regex
	 * @param genre
	 * using regex to change attributes of the genre
	 */
	public void gameAttributesChange(String regex, String genre)
	{
		String gameName;
		for(Game g : list)
		{
			
			gameName = g.getName();			
			if(RegexUtil.matches(regex, gameName) == true)
			{
				g.setGenre(genre);
			}
		}
		
	}
	/**
	 * @param stream
	 * @param regex
	 * @throws IOException
	 * using regex  to copy games to another file 
	 */
	public void copyFiles(RandomAccessFile stream, String regex) throws IOException
	{
		String gameName;
		for(Game g : list)
		{
			
			gameName = g.getName();			
			if(RegexUtil.matches(regex, gameName) == true)
			{
				g.write(stream);
			}
		}
	}
}








