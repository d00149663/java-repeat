import java.io.IOException;
import java.io.RandomAccessFile;


/**
 * @author masood
 * CA1 Repeat AOOD
 *
 */
public class Game {
	
	private String name, genre;
	private int players, age;
	private  boolean network;
	public static int GAME_LENGTH = 113;
	

	/**
	 * constructor
	 */
	public Game() {
		this.name = " ";
		this.genre ="";
		this.players = 1;
		this.age = 12;
		this.network = false;
		}
	/**
	 * @param name
	 * @param genre
	 * @param players
	 * @param age
	 * @param network
	 */
	public Game(String name, String genre, int players, int age, boolean network) {
		this.name = name;
		this.genre = genre;
		this.players = players;
		this.age = age;
		this.network = network;
	}

	/**
	 * @Getters and setters
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getPlayers() {
		return players;
	}
	public void setPlayers(int players) {
		this.players = players;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public boolean isNetwork() {
		return network;
	}
	public void setNetwork(boolean network) {
		this.network = network;
	}
	/**
	 * print tostring
	 */
	public void print()
	{
		System.out.println(this.toString());
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Game [Name = " + name + ", Genre = " + genre + ", Players = "
				+ players + ", Age = " + age + ", Network = " + network + "]";
	}
	/**
	 * @return string
	 */
	public String getContent() {
		
		return null;
	}	
	//read at the current position of the read/write pointer
	/**
	 * @param stream
	 * @throws IOException
	 * read RAF
	 */
	public  void read(RandomAccessFile stream) throws IOException 
	{
		
		this.name = StringUtility.unpad(stream.readUTF(), '$');		
		this.genre = StringUtility.unpad(stream.readUTF(), '$');	
		this.players =stream.readInt();
		this.age = stream.readInt();
		this.network =stream.readBoolean();		
	}
	/**
	 * @param stream
	 * @throws IOException
	 * write RAF
	 */
	public void write(RandomAccessFile stream) throws IOException 
	{ 
		stream.writeUTF(StringUtility.pad(name, '$', 50));//52
		stream.writeUTF(StringUtility.pad(genre, '$', 50));//52
		stream.writeInt(players);	//4
		stream.writeInt(age);	//4
		stream.writeBoolean(network);//1		
		//52+52+4+4+1=113bytes
	}
}
