
public class StringUtility 
{
	public static String pad(String data, char padChar, int padLength)
	{
		int padDifference = padLength - data.length();		
		StringBuilder strBuilder = new StringBuilder(data);		
		for(int i = 0; i < padDifference; i++)
		{
			strBuilder.append(padChar);
		}		
		return strBuilder.toString();
	}
	
	public static String unpad(String data, char padChar)
	{
		return data.substring(0, data.indexOf(padChar));
	}

}




