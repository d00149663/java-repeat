import java.io.IOException;

public class MainApp 
{

	/**
	 * @param args
	 * main app
	 */
	public static void main(String[] args) 
	{		
		MainApp theApp = new MainApp();	
		try {
			theApp.start();
			System.out.println("App finish...");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @throws IOException
	 * start main
	 */
	private void start() throws IOException  	
		{
			menu.menu();
		}
	}

/**
 * @throws IOException
 * these were the tests made before making menu
 */
		
	/*
//		String fileName = "c:/temp/games.txt";
//		String copyfileName = "c:/temp/copiedGames.txt";
		
//		try {
//			theApp.start(fileName);
////			theApp.start2(fileName);
////			theApp.start3(fileName, copyfileName);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
	*/

/*
//	private void start(String fileName) throws IOException  
	
//	{	
//		MyRandomAccessStream stream= new MyRandomAccessStream(fileName, "rw");
////		
//		stream.open();
//		Game g1 = new Game();
//		g1.read(stream.getStream());
//		System.out.println(g1.toString());
////		
//		GameStore games = new GameStore();
//		
//		System.out.println("Populating game shop");
//		games.populate();
//		games.print();
////		
//		System.out.println("added");
//		games.add(g1);

////		games.print();		
//		games.saveAll(stream.getStream());
//		stream.close();
//
////		GameStore games2 = new GameStore();
////		games2.loadAll(stream.getStream());
//		System.out.println(".............");
////		games.print();
//		System.out.println("\n");
//		//games.delete("Superman");
//		stream.open();
//		System.out.println("LOading games");
//		games.loadAll(stream.getStream());
//		games.edit(3, stream.getStream());
//	
//		System.out.println("\n");
////		games.print();
//		games.printOne("Batman");
////		games.saveAll(stream);
////		Game g = games.gameSearchRegix(stream);
////		System.out.println("THE GAME: "+g);
//		
//		stream.close();
//			
//	}
	private void start2(String fileName) throws IOException  
	{	
		MyRandomAccessStream stream= new MyRandomAccessStream(fileName, "rw");
		stream.open();

		GameStore games = new GameStore();
		
		System.out.println("Populating game shop");
//		games.populate();
		games.loadAll(stream.getStream());
		games.print();
		String regex = ".*man";
//		games.gameSearchRegex(regex);
		games.gameAttributesChange(regex, "Arcade");
		stream.getStream().seek(0);
		games.saveAll(stream.getStream());
		stream.close();
		
	}
	private void start3(String fileName, String copyFile) throws IOException  
	{	
		MyRandomAccessStream stream= new MyRandomAccessStream(fileName, "rw");
		stream.open();

		GameStore games = new GameStore();
		
		//System.out.println("Populating game shop");
//		games.populate();
		games.loadAll(stream.getStream());
		games.print();
		String regex = ".*man";
		games.gameSearchRegex(regex);
		games.gameAttributesChange(regex, "Arcade");
		stream.getStream().seek(0);
		games.saveAll(stream.getStream());
		stream.close();
		
		MyRandomAccessStream cpStream= new MyRandomAccessStream(copyFile, "rw");
		cpStream.open();
		games.copyFiles(cpStream.getStream(), regex);
		cpStream.close();
		}
		}
*/
		
	

