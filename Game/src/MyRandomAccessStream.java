import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;


public class MyRandomAccessStream 
{
	private String fileName, filePermissions;
	protected RandomAccessFile stream;
	
	/**
	 * @param fileName
	 * @param filePermissions
	 */
	public MyRandomAccessStream(String fileName, String filePermissions)
	{
		this.fileName = fileName;
		this.filePermissions = filePermissions;
	}
	
	public void open()
	{
		try 
		{
			this.stream = new RandomAccessFile(fileName, filePermissions);
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("Error opening: " + fileName
					+ "\n" + e.getMessage());
		}
	}
	public void close()
	{
		try 
		{
			stream.close();
		} 
		catch (IOException e) 
		{
			System.out.println("Error closing: " + fileName
					+ "\n" + e.getMessage());
		}
	}

	//returns a handle to the input/output stream
	public RandomAccessFile getStream() 
	{
		return stream;
	}
	
	//returns current position of the read/write pointer
	public long getFilePointerOffset()
	{
		try 
		{
			return stream.getFilePointer();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return -1;
	}
}





















